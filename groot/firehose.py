# -*- coding: utf-8 -*-

# NOTE: this code is heavily inspired by the following:
#   blog:   https://alephsecurity.com/2018/01/22/qualcomm-edl-1/
#   github: https://github.com/alephsecurity/firehorse.git
#
# Thank you to Roee Hay (@roeehay) & Noam Hadad for their blog.
# Please check it out, it has heaps more information than I have.
#
# This file used the firehorse codebase as a referenece to using
# 'fh_loader.exe' and 'qsaharaserver.exe'.
#

import os
import sys
import time
import struct
import binascii
import tempfile
import subprocess

from elftools.elf.elffile import ELFFile
from elftools.elf.constants import P_FLAGS
from elftools.elf.descriptions import describe_p_flags

from unicorn import Uc, UC_ARCH_ARM, UC_ARCH_ARM64, UC_MODE_ARM
from unicorn.arm_const import *
from unicorn.arm64_const import *

from capstone import Cs, CS_ARCH_ARM, CS_ARCH_ARM64, CS_MODE_ARM

from .serial import Serial

def uc_get_reg(uc_arch, reg_name):
    reg_name = reg_name.upper().strip()
    if UC_ARCH_ARM == uc_arch:
        import unicorn.arm_const
        reg = 'UC_ARM_REG_' + reg_name
        module = unicorn.arm_const
    elif UC_ARCH_ARM64 == uc_arch:
        import unicorn.arm64_const
        reg = 'UC_ARM64_REG_' + reg_name
        module = unicorn.arm64_const
    else:
        raise NotImplementedError
    reg = getattr(module, reg, None)
    assert reg is not None
    return reg


class Firehose(object):
    FILE_ID_PROGRAMMER = 13
    MAX_PEEK_SIZE = 512  # randomly chosen
    PEEK_ARM_XML = b'<?xml version="1.0" ?>\n<data>\n<peek address64="ADDR" SizeInBytes="SIZE"/>\n\n</data>'
    POKE_ARM_XML = b'<?xml version="1.0" ?>\n<data>\n<poke address64="ADDR" SizeInBytes="SIZE" value="VALUE"/>\n</data>'
    PEEK_ARM64_XML = b'<?xml version="1.0" ?>\n<data>\n<peek address64="ADDR" size_in_bytes="SIZE"/>\n</data>'
    POKE_ARM64_XML = b'<?xml version="1.0" ?>\n<data>\n<poke address64="ADDR" size_in_bytes="SIZE" value64="VALUE"/>\n</data>'
    PEEK_XML = {
        'arm': PEEK_ARM_XML,
        'arm64': PEEK_ARM64_XML,
    }
    POKE_XML = {
        'arm': POKE_ARM_XML,
        'arm64': POKE_ARM64_XML,
    }

    @classmethod
    def command(cls, args):
        try:
            p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except FileNotFoundError as e:
            raise FileNotFoundError("could not find binary '{}' in the PATH".format(args[0]))
        stdout, stderr = p.communicate()
        out = '{}\n{}'.format(stdout.decode(), stderr.decode())
        return out

    def __init__(self, programmer=None, port=None):
        self.port = port
        self.programmer = programmer
        self._arch = None
        self._programmer_format = None
        self._entry = None
        self._stack = None
        self._stack_size = None
        self._vbar = None
        self._el = None
        self._rwx = None
        self._rwx_size = None

    def find_serial_port(self):
        for serial in Serial.comports():
            description = serial.description.lower()
            if description.find('qualcomm') == -1:
                continue
            if description.find('qdloader') == -1:
                continue
            self.port = serial.port
            return
        raise Exception("could not find firehose serial device")

    @property
    def device_path(self):
        return '\\\\.\\{}'.format(self.port)

    @property
    def in_sahara_mode(self):
        try:
            self.nop()
            return True
        except:
            pass
        return False

    def programmer_parse(self, path):
        self.programmer = None
        self._arch = None
        self._elf_format = None
        self._entry = None
        self._stack = None
        self._stack_size = None
        self._vbar = None
        self._el = None
        self._rwx = None
        self._rwx_size = None
        with open(path, 'rb') as handle:
            # ELF
            elffile = ELFFile(handle)
            header = elffile.header
            arch = 'arm'
            if 'EM_AARCH64' == header['e_machine']:
                arch = 'arm64'
            e_ident = header['e_ident']
            elf_format = 32
            if 'ELFCLASS64' == e_ident['EI_CLASS']:
                elf_format = 64
            entry = header['e_entry']
            assert 0 != entry
            self.programmer = path

            # Emulate
            if arch == 'arm64':
                uc_arch = UC_ARCH_ARM64
                uc_reg_pc = UC_ARM64_REG_PC
                uc_reg_sp = UC_ARM64_REG_SP
                uc_reg_prefix = 'UC_ARM64_REG_'
                cs_arch = CS_ARCH_ARM64
            else:
                uc_arch = UC_ARCH_ARM
                uc_reg_pc = UC_ARM_REG_PC
                uc_reg_sp = UC_ARM_REG_SP
                uc_reg_prefix = 'UC_ARM_REG_'
                cs_arch = CS_ARCH_ARM
            md = Cs(cs_arch, CS_MODE_ARM)
            mu = Uc(uc_arch, UC_MODE_ARM)
            for segment in elffile.iter_segments():
                if 'PT_LOAD' != segment['p_type']:
                    continue
                if ( P_FLAGS.PF_X & segment['p_flags']) == 0:
                    continue
                offset = segment['p_offset']
                size = segment['p_filesz']
                handle.seek(offset)
                data = handle.read(size)
                assert len(data) == size
                vaddr = segment['p_vaddr']
                vsize = segment['p_memsz']
                if vsize & 0xfff != 0:
                    vsize = vsize & ~0xfff
                    vsize = vsize + 0x1000
                try:
                    mu.mem_map(vaddr, vsize)
                    mu.mem_write(vaddr, data)
                except:
                    pass
            mu.reg_write(uc_reg_pc, entry)
            stack = None
            vbar = None
            el = None
            while not stack or not vbar or not el:
                pc = mu.reg_read(uc_reg_pc)
                code = mu.mem_read(pc, 4)
                ins = list(md.disasm(code, pc))[0]
                # print("0x%x:\t%s\t%s" %(ins.address, ins.mnemonic, ins.op_str))
                mu.emu_start(mu.reg_read(uc_reg_pc), 0, count=1)
                if 0 != mu.reg_read(uc_reg_sp):
                    stack = mu.reg_read(uc_reg_sp)
                if ins.op_str.startswith('vbar_el'):
                    reg = ins.op_str.split(',')[1]
                    uc_reg = uc_get_reg(uc_arch, reg)
                    vbar = mu.reg_read(uc_reg)
                    el = int(ins.op_str[len('vbar_el'): len('vbar_el') + 1], 10)
                if ins.mnemonic == 'mcr' and ins.op_str.startswith('p15'):
                    reg = ins.op_str.split(',')[2]
                    uc_reg = uc_get_reg(uc_arch, reg)
                    vbar = mu.reg_read(uc_reg)
                    # FIXME: only arm32 I know is nokia 6. Assume EL3
                    el = 3

            # Stack size
            stack_size = None
            for segment in elffile.iter_segments():
                vaddr = segment['p_vaddr']
                vsize = segment['p_memsz']
                vaddr_min = vaddr
                vaddr_max = vaddr + vsize
                if vaddr + vsize == stack:
                    stack_size = vsize
                    break
                if stack > vaddr_min and stack < vaddr_max:
                    stack_size = stack - vaddr
            if not stack_size:
                raise Exception("Failed to find stack size")

            # RWX
            rwx = 0
            rwx_size = 0
            for segment in elffile.iter_segments():
                if 'PT_LOAD' != segment['p_type']:
                    continue
                # print(describe_p_flags(segment['p_flags']))
                if ( P_FLAGS.PF_R & segment['p_flags']) == 0:
                    continue
                if ( P_FLAGS.PF_W & segment['p_flags']) == 0:
                    continue
                if ( P_FLAGS.PF_X & segment['p_flags']) == 0:
                    continue
                vaddr = segment['p_vaddr']
                vsize = segment['p_memsz']
                if vsize & 0xfff != 0:
                    vsize = vsize & ~0xfff
                    vsize = vsize + 0x1000
                if vsize > rwx_size:
                    rwx = vaddr
                    rwx_size = vsize

        # Outcome
        self._arch = arch
        self._elf_format = elf_format
        self._entry = entry
        self._stack = stack
        self._stack_size = stack_size
        self._vbar = vbar
        self._el = el
        self._rwx = rwx
        self._rwx_size = rwx_size

    @property
    def arch(self):
        if self._arch is None:
            self.programmer_parse(self.programmer)
        return self._arch

    @property
    def elf_format(self):
        if self._elf_format is None:
            self.programmer_parse(self.programmer)
        return self._elf_format

    @property
    def entry(self):
        if self._entry is None:
            self.programmer_parse(self.programmer)
        return self._entry

    @property
    def stack(self):
        if self._stack is None:
            self.programmer_parse(self.programmer)
        return self._stack

    @property
    def stack_size(self):
        if self._stack_size is None:
            self.programmer_parse(self.programmer)
        return self._stack_size

    @property
    def vbar(self):
        if self._vbar is None:
            self.programmer_parse(self.programmer)
        return self._vbar

    @property
    def el(self):
        if self._el is None:
            self.programmer_parse(self.programmer)
        return self._el

    @property
    def rwx(self):
        if self._rwx is None:
            self.programmer_parse(self.programmer)
        return self._rwx

    @property
    def rwx_size(self):
        if self._rwx_size is None:
            self.programmer_parse(self.programmer)
        return self._rwx_size

    def qsaharaserver(self, args):
        assert self.port.startswith('COM')
        new_args = ['qsaharaserver', '-p', self.device_path]
        new_args.extend(args)
        args = new_args
        out = self.command(args)
        if 'error' in out.lower():
            raise Exception('QSaharaServer command failed')
        return out

    def send_programmer(self, path=None):
        if path:
            self.programmer_parse(path)
        path = os.path.realpath(self.programmer)
        assert os.path.exists(path)
        search_path = os.path.dirname(path)
        programmer = os.path.basename(path)
        args = [
            "-s", "{}:{}".format(self.FILE_ID_PROGRAMMER, programmer),
            "-b", "{}\\\\".format(search_path),
            "-k",
        ]
        try:
            self.qsaharaserver(args)
        except:
            if not self.in_sahara_mode:
                raise

    def firehose(self, args):
        assert self.port.startswith('COM')
        new_args = ['fh_loader', '--port={}'.format(self.device_path), '--noprompt']
        new_args.extend(args)
        args = new_args
        out = self.command(args)
        if not '{all finished successfully}' in out.lower():
            raise Exception('fh_loader command failed')
        return out

    def nop(self):
        out = self.firehose(['--nop'])
        if not 'got the ping response' in out.lower():
            raise Exception('firehose nop command failed')
        return out

    def send_xml_file(self, path):
        path = os.path.realpath(path)
        assert os.path.exists(path)
        search_path = os.path.dirname(path)
        filename = os.path.basename(path)
        args = [
            "--search_path={}".format(search_path),
            "--sendxml={}".format(filename),
        ]
        out = self.firehose(args)
        return out

    def send_xml(self, text):
        if not isinstance(text, bytes):
            data = text.encode('ascii')
        else:
            data = text
        temp_xml_file = None
        try:
            temp_xml_file = tempfile.mktemp(suffix='.xml')
            with open(temp_xml_file, 'wb') as handle:
                handle.write(data)
            out = self.send_xml_file(temp_xml_file)
            return out
        finally:
            if temp_xml_file and os.path.exists(temp_xml_file):
                os.remove(temp_xml_file)

    def peek(self, addr, size):
        template_xml = self.PEEK_XML[self.arch]
        addr_hex = hex(addr).replace('L', '').encode('ascii')
        size_hex = hex(size).replace('L', '').encode('ascii')
        xml = template_xml.replace(b'ADDR', addr_hex).replace(b'SIZE', size_hex)
        out = self.send_xml(xml)
        data = b''
        for line in out.split('\n'):
            line = line.lower().strip()
            if line.find('target said:') == -1:
                continue
            line = line.replace("'", '').replace("'", '')
            line = line.split('target said:')[1]
            line = line.strip()
            if not line.startswith('0x'):
                continue
            line = line.replace('0x', '').replace(' ', '')
            line = line.encode('ascii')
            line = binascii.a2b_hex(line)
            data += line
        assert len(data) == size
        return data

    def read_memory(self, addr, size):
        if size == 0:
            return b''
        if size > self.MAX_PEEK_SIZE:
            data = b''
            peeks = int(size / self.MAX_PEEK_SIZE)
            for i in range(0, peeks * self.MAX_PEEK_SIZE, self.MAX_PEEK_SIZE):
                data += self.peek(addr + i, self.MAX_PEEK_SIZE)
            recd = peeks * self.MAX_PEEK_SIZE
            assert size >= recd
            data += self.read_memory(addr + recd, size - recd)
            assert len(data) == size
            return data
        return self.peek(addr, size)

    def poke(self, addr, data, verify=False):
        assert isinstance(data, bytes)
        if len(data) == 8:
            # write 64-bit values in one hit
            size = 8
            value = struct.unpack('<Q', data)[0]
        elif len(data) == 4:
            # write 32-bit values in one hit
            size = 4
            value = struct.unpack('<I', data)[0]
        elif len(data) == 1:
            # a way to write a single byte
            size = 1
            value = struct.unpack('<B', data)[0]
        else:
            raise NotImplementedError("poke only support 1, 4 and 8 byte writes")
        template_xml = self.POKE_XML[self.arch]
        addr_hex = hex(addr).replace('L', '').encode('ascii')
        size_hex = hex(size).replace('L', '').encode('ascii')
        value_hex = hex(value).replace('L', '').encode('ascii')
        xml = template_xml.replace(b'ADDR', addr_hex).replace(b'SIZE', size_hex).replace(b'VALUE', value_hex)
        out = self.send_xml(xml)
        assert 'all finished successfully' in out.lower()
        if verify:
            result = self.peek(addr, len(data))
            assert result == data
        return

    def write_memory(self, addr, data):
        assert isinstance(data, bytes)
        if len(data) == 0:
            return
        elif len(data) < 8:
            # write small amount of odd-shaped data
            for i in range(0, len(data)):
                self.poke(addr + i, data[i: i + 1])
            return
        # write buffers (32-bit values at a time)
        dwords = int(len(data) / 4)
        for i in range(0, dwords * 4, 4):
            self.poke(addr + i, data[i: i + 4])
        i = dwords * 4
        if i < len(data):
            self.write_memory(addr + i, data[i: i + 4])
        return
