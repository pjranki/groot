# -*- coding: utf-8 -*-

import os
import sys

from .firehose import Firehose

def test(device, addr):
    print(device.read_memory(addr, 8))
    device.write_memory(addr, b'hellotw3')
    print(device.read_memory(addr, 8))
    device.write_memory(addr, b'XYZ')
    print(device.read_memory(addr, 8))
    print(device.read_memory(addr, 16))
    device.write_memory(addr, b'HELLOWORLD')
    print(device.read_memory(addr, 16))

def main():
    if len(sys.argv) <= 1:
        print("{} <prog_emmc_firehose_8937_lite.mbn>".format(os.path.basename(__file__)))
        sys.exit(1)
    programmer=sys.argv[1]
    assert os.path.exists(programmer)
    device = Firehose(programmer)
    print('entry @ {}'.format(hex(device.entry)))
    print('stack @ {}, {} bytes'.format(hex(device.stack), hex(device.stack_size)))
    print('vbar_el{} @ {}'.format(device.el, hex(device.vbar)))
    print('rwx @ {}, {} bytes'.format(hex(device.rwx), hex(device.rwx_size)))
    #device.find_serial_port()
    #device.send_programmer()
    #spare_space = device.stack - device.stack_size + 100
    #test(device, device.stack)

if __name__ == '__main__':
    main()
