# -*- coding: utf-8 -*-

import serial.tools.list_ports  # pyserial

class Serial(object):

    def __init__(self, port=None, description=None):
        self.port = port
        self.description = description

    @classmethod
    def comports(cls):
        found_ports = list()
        ports = list(serial.tools.list_ports.comports())
        for port in ports:
            description = port.description.lower()
            port = port.device.upper()
            found_ports.append(Serial(port, description))
        return found_ports
