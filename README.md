# groot

Dumping ground for python utilities. Hopefully it grows into something useful.

## ops
Extract encrypt oneplus firmware files (ops)

```
python3 -m groot.ops firmware.ops extract_to_this_folder
```

## firehose
Requires QualComm QPST tools to be installed.

Add "C:\Program Files (x86)\Qualcomm\QPST\bin" to your PATH.

Make sure you have the following binaries in your PATH:
```
fh_loader.exe
qsaharaserver.exe
```

The firehose code is heavily inspired by the following:
https://alephsecurity.com/2018/01/22/qualcomm-edl-1/
https://github.com/alephsecurity/firehorse.git

Thank you to Roee Hay (@roeehay) & Noam Hadad for their blog.
Please check it out, it has heaps more information than I have.

## oneplus5
Demo code for reading/writing OnePlus 5 memory in EDL mode.

Get a copy of the firmware (hint: Google "oneplus5 msmdownloader V4.0").

Unzip and locate the .ops file, extract it with the ops script (in this repo).

You should now have the "prog_ufs_firehose_8998_ddr.elf" binary.

Power off your OnePlus 5, connect to a pc, Hold Power + Vol-UP + Home/Fingerprint button.

Then run:
```
python3 -m groot.oneplus5 prog_ufs_firehose_8998_ddr.elf
```

This script reads/writes a few bytes at the end of the stack as a POC.
